const routes = [
    {
      path: '/:room',
      component: () => import('../components/Board.vue')
    }
  ]
  
  export default routes
  